package frontend;
import backend.die;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
public class DieApplication extends Application{
	private die game = new die();
	public void start(Stage stage) {
		Group root = new Group(); 

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		
		//layout
		ButtonBar buttonbar = new ButtonBar();
		Messages msg = new Messages();
		VBox overall = new VBox();
		overall.getChildren().addAll(buttonbar, msg);
		root.getChildren().add(overall);
		
		//Events
		EvenOddClick clicks = new EvenOddClick(buttonbar, msg, game);
		clicks.click();
		
		stage.setScene(scene);
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }

}
