package frontend;
import backend.die;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
public class DieChoice implements EventHandler<ActionEvent>{
	private ButtonBar buttons;
	private Messages msg;
	private die game;
	private int bet;
	private int choice;
	
	public DieChoice(ButtonBar buttons, Messages msg, die game, int choice) {
		this.buttons = buttons;
		this.msg = msg;
		this.game = game;
		this.choice = choice;
	}
	
	public void handle(ActionEvent e) {
		try {
			this.bet = Integer.parseInt(msg.getBet().getText());
			msg.getMoney().setText(Integer.toString(game.getCurrentMoney()));
			game.setBet(this.bet);
			game.setChoice(choice);
			String result = game.roll();
			msg.getMoney().setText(Integer.toString(game.getCurrentMoney()));
			msg.getResult().setText(result);
		}
		catch(Exception a){
			msg.getError().setText("not a valid number");
			msg.getResult().setText("");
		}
	}
}
