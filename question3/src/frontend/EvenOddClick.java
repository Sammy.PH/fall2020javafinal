package frontend;
import backend.die;
public class EvenOddClick {
	private ButtonBar buttons;
	private Messages msg;
	private die game;
	
	public EvenOddClick(ButtonBar buttons, Messages msg, die game) {
		this.buttons = buttons;
		this.msg = msg;
		this.game = game;
	}
	
	public void click() {
		DieChoice evenChoice = new DieChoice(buttons, msg, game, 2);
		DieChoice oddChoice = new DieChoice(buttons, msg, game, 1);
		buttons.getEvenButton().setOnAction(evenChoice);
		buttons.getOddButton().setOnAction(oddChoice);
	}
}
