package frontend;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
public class Messages extends HBox{
	private TextField bet = new TextField("bet");
	private TextField money = new TextField();
	private TextField result = new TextField("result");
	private TextField error = new TextField();
	
	public Messages() {
		this.getChildren().addAll(bet,money, result, error);
	}
	
	public TextField getMoney() {
		return money;
	}
	
	public TextField getResult() {
		return result;
	}
	
	public TextField getError() {
		return error;
	}
	
	public TextField getBet() {
		return bet;
	}
}
