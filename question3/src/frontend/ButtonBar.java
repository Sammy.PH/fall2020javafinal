package frontend;
import javafx.scene.control.Button;
import javafx.scene.layout.*;
public class ButtonBar extends HBox{
	private Button even = new Button("Even");
	private Button odd = new Button("Odd");
	public ButtonBar() {
		this.getChildren().addAll(even, odd);
	}
	
	public Button getEvenButton() {
		return even;
	}
	
	public Button getOddButton() {
		return odd;
	}
}
