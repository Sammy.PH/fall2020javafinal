package backend;
import java.util.Random;
public class die {
	private int currentMoney = 250;
	private int bet;
	private int isChoiceEven;// 2 for even, 1 for odd
	private boolean evenOrOdd;//true for even, false for odd
	
	//Method to roll the die
	public String roll() {
		isEvenOrOdd();
		if(evenOrOdd && isChoiceEven == 2) {
			this.currentMoney = this.currentMoney + this.bet;
			return "You won " + this.bet + ", it was even";
		}
		else if(!evenOrOdd && isChoiceEven == 1) {
			this.currentMoney = this.currentMoney + this.bet;
			return "You won " + this.bet + ", it was odd";
		}
		else {
			this.currentMoney = this.currentMoney - this.bet;
			return "You lost " + bet;
		}
	}
	
	//method to decide if its even or odd
	public void isEvenOrOdd() {
		Random random = new Random();
		int num = random.nextInt(2) + 1;
		if(num % 2 == 0) {
			this.evenOrOdd = true; // true for even
		}
		else {
			this.evenOrOdd = false;
		}
	}
	
	//Get methods
	public int getCurrentMoney() {
		return this.currentMoney;
	}
	
	public void setBet(int num) {
		this.bet = num;
	}
	
	public void setChoice(int num) {
		this.isChoiceEven = num;
	}
}
